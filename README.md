# postscriptum-postpdf

An utility to process the PDF output of Chromium by adding the bookmarks, croping the pages and setting some metadata.

It is based on [PoDoFo](http://podofo.sourceforge.net/), a C++ library to work with the PDF file format.

## Postscriptum

Postscriptum is a solution to produce PDF from HTML with the help of CSS Print.

Please refer to [__https://postscriptum.app__](https://postscriptum.app) for a global picture of the project.

## Issues

Please report any issues on the [*postscriptum-core* project](https://gitlab.com/postscriptum.app/core/-/issues).

## License

[GNU Lesser General Public License](http://www.gnu.org/licenses/lgpl-3.0.html)[](http://www.gnu.org/licenses/lgpl-3.0.html)

## Build

This program must be easily embedded in the postscriptum packaging. The build therefore intends to produce a static binary.

The build is not supported on Windows or macOS but can be achieved with
[Docker](https://www.docker.com/) or [WSL](https://docs.microsoft.com/en-us/windows/wsl/).

### Dependencies

- build-essentials (gcc, binutils, etc...)
- cmake
- mingw-w64-i686 (to cross-build)

On an Ubuntu : `apt-get install -y build-essential cmake g++-mingw-w64-i686 binutils-mingw-w64-i686`

There is no need to install PoDoFo and his dependencies. The CMake build handles them.

### Instructions

- Linux and macOS : `scripts/build.sh`
- Windows (mingw32 crossbuild) : `scripts/build.sh win32 cmake/Toolchain-i686-w64-mingw32.cmake`

The binaries are produced in the `build` directory.

### With Docker

- Linux : `docker run --rm -v "$PWD":/work multiarch/crossbuild /work/scripts/build.sh`
- Windows : `docker run --rm -v "$PWD":/work -e CROSS_TRIPLE=i686-w64-mingw32 multiarch/crossbuild /work/scripts/build.sh win32 /work/cmake/Toolchain-i686-w64-mingw32.cmake`
- macOS : `docker run --rm -v "$PWD":/work -e CROSS_TRIPLE=x86_64-apple-darwin multiarch/crossbuild /work/scripts/build.sh darwin /work/cmake/Toolchain-x86_64-apple-darwin14.cmake`

On Windows, replace `$PWD` by `%CD%`.

