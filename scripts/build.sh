#!/bin/bash
set -e
baseDir=$(cd "$(dirname "$0")/.." && pwd -P)

case $OSTYPE in
  linux*) hostPlatform="linux";;
  darwin*)  hostPlatform="darwin";;
  *) hostPlatform=$OSTYPE;;
esac

platform=${1:-$hostPlatform}
buildDir="$baseDir"/build/"$platform"
distDir="$baseDir"/dist/"$platform"
rm -rf "$buildDir" "$distDir"
mkdir -p "$buildDir" "$distDir"
toolchainArg=${2:+"-DCMAKE_TOOLCHAIN_FILE=$(readlink -f "$2")"}
cd "$buildDir"
cmake -DCMAKE_BUILD_TYPE=MINSIZEREL $toolchainArg ../..
make
mv "$buildDir"/postscriptum-postpdf* "$distDir"
