#!/bin/bash
set -e
baseDir=$(cd "$(dirname "$0")/.." && pwd -P)

rm -rf "$baseDir"/build "$baseDir"/dist
dockerRun () {
	docker run -u $(id -u ${USER}):$(id -g ${USER}) --rm -v "$baseDir":/work $*
}

dockerRun multiarch/crossbuild /work/scripts/build.sh
dockerRun -e CROSS_TRIPLE=arm-linux-gnueabi multiarch/crossbuild /work/scripts/build.sh linux_arm /work/cmake/Toolchain-arm-linux-gnueabi.cmake
dockerRun -e CROSS_TRIPLE=i686-w64-mingw32 multiarch/crossbuild /work/scripts/build.sh win32 /work/cmake/Toolchain-i686-w64-mingw32.cmake
dockerRun -e CROSS_TRIPLE=x86_64-apple-darwin multiarch/crossbuild /work/scripts/build.sh darwin /work/cmake/Toolchain-x86_64-apple-darwin.cmake
