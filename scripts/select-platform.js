const fs = require('fs');
const path = require('path');

let selected = process.argv[2];
if (!selected) {
	selected = process.platform;
	if (selected == "linux" && process.arch == "arm") selected += "_arm";
}

for (const platform of [ 'linux', 'linux_arm', 'win32', 'darwin']) {
	const platformDir = path.join(__dirname, '..', 'dist', platform);
	if (platform != selected) {
		fs.rmSync(platformDir, { recursive: true, force: true });
	} else if (platform != 'win32') {
		fs.chmodSync(path.join(platformDir, 'postscriptum-postpdf'), 0o755);
	}
}