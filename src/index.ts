import type {Readable, Writable} from 'node:stream';
import * as childProcess from 'node:child_process';
import * as fs from 'node:fs';
import * as path from 'node:path';
import * as os from 'node:os';

export type Size = [number, number];

let platform = process.platform;
if (platform == "linux" && process.arch == "arm") platform += "_arm";
let executablePath = `${__dirname}/${platform}/postscriptum-postpdf`;
if (platform == "win32") executablePath += '.exe';
export {executablePath};

export async function postPDF(inputStream: Buffer | Readable, config: any, outputStream: Writable): Promise<void> {
	const tempDir = await fs.promises.mkdtemp(path.join(os.tmpdir(), 'postscriptum-postpdf-'));
	// Cannot pass the config by argument: too long on Windows (ENAMETOOLONG)
	const configFile = path.resolve(tempDir, 'config.json');
	await fs.promises.writeFile(configFile, JSON.stringify(config));
	return new Promise<void>((resolve, reject) => {
		const postpdf = childProcess.spawn(executablePath, ['-', configFile, '-']);

		let errorOutput = "";
		postpdf.stderr.on('data', (data) => errorOutput += data);
		outputStream.once("error", reject);

		postpdf.once('close', (code, signal) => {
			fs.rmSync(tempDir, { recursive: true });
			if (code || signal || errorOutput) reject(new Error(`The post processing of the PDF did not stop properly (${signal || code}):\n${errorOutput}`));
			else if (outputStream.writableFinished) resolve();
			else outputStream.on('close', resolve);
		});

		if ('pipe' in inputStream) inputStream.pipe(postpdf.stdin);
		else postpdf.stdin.end(inputStream);
		postpdf.stdout.pipe(outputStream);
	});
}
