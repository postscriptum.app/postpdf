#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <cmath>
#include <podofo/podofo.h>
#include "json.h"

#ifdef _WIN32
  #include <io.h>
  #include <fcntl.h>
#endif

using namespace PoDoFo;
using namespace std;

const string VERSION("1.0.0");

void printHelp() {
  cout << "Usage: postscriptum-postpdf <input.pdf> <config.json> <output.pdf>" << endl;
}

void bookmarksToOutlines(PdfMemDocument * pDoc, PdfOutlineItem * pParent, Json::Value& bookmarks, PdfObject* pDests = 0) {
  if (!pDests) pDests = pDoc->GetCatalog()->GetIndirectKey(PdfName("Dests"));
  if (!pDests) return;

  for (int i = 0; i < bookmarks.size(); i++) {
    Json::Value& bookmark = bookmarks[i];
    PdfString title(reinterpret_cast<const pdf_utf8*>(bookmark["label"].asCString()));
    PdfObject* pDest = pDests->GetIndirectKey(PdfName(bookmark["dest"].asString()));
    if (pDest) {
      PdfOutlineItem * outlineItem = pParent->CreateChild(title, PdfDestination(pDest, pDoc));
      if (bookmark["children"].isArray()) {
        bookmarksToOutlines(pDoc, outlineItem, bookmark["children"], pDests);
      }
    }
  }
}

PdfString json2PdfString(Json::Value json) {
  return PdfString(reinterpret_cast<const pdf_utf8*>(json.asCString()));
}


int main (int argc, char* argv[]) {
  PdfError::EnableDebug (false);

  try {
    if (argc == 2 && string(argv[1]) == "-v") {
      cout << VERSION << endl;
      exit (0);
    } else if (argc != 4) {
      printHelp();
      exit (-1);
    }

    string inputFile(argv[1]);
    string configFile(argv[2]);
    string outputFile(argv[3]);

    PdfMemDocument doc;
    if (inputFile == "-") {
      cin >> noskipws;
      #ifdef _WIN32
        _setmode(_fileno(stdin), _O_BINARY);
        cin.sync_with_stdio();
      #endif
      // Podofo requires a seekable stream: bufferization of stdin
      istream* istream = new istringstream(string(istreambuf_iterator<char>(cin), istreambuf_iterator<char>()), ios::binary);
      doc.LoadFromDevice(PdfRefCountedInputDevice(new PdfInputDevice(istream)));
    } else {
      doc.Load(inputFile.c_str());
    }

    Json::Value config;
    Json::CharReaderBuilder readerBuilder;
    bool configParsed;
    string parseErrrors;
    if (configFile[0] == '{') {
      Json::CharReader* reader = readerBuilder.newCharReader();
      configParsed = reader->parse(configFile.c_str(), configFile.c_str() + configFile.size(), &config, &parseErrrors);
      delete reader;
    } else {
      ifstream configInput (configFile.c_str());
      if (configInput.fail()) {
        cerr << "Unable to open the configuration." << endl;
        return 1;
      }
      configParsed = parseFromStream(readerBuilder, configInput, &config, &parseErrrors);
    }
    if (!configParsed) {
      cerr << "An error occured while parsing the JSON configuration." << endl << parseErrrors << endl;
      return 1;
    }

    PdfInfo* pdfInfo = doc.GetInfo();
    Json::Value jsonInfo = config["info"];
    if (jsonInfo.isObject()) {
      if (jsonInfo["title"].isString()) pdfInfo->SetTitle(json2PdfString(jsonInfo["title"]));
      if (jsonInfo["author"].isString()) pdfInfo->SetAuthor(json2PdfString(jsonInfo["author"]));
      if (jsonInfo["subject"].isString()) pdfInfo->SetSubject(json2PdfString(jsonInfo["subject"]));
      if (jsonInfo["keywords"].isString()) pdfInfo->SetKeywords(json2PdfString(jsonInfo["keywords"]));
      if (jsonInfo["creator"].isString()) pdfInfo->SetCreator(json2PdfString(jsonInfo["creator"]));
      if (jsonInfo["producer"].isString()) pdfInfo->SetProducer(jsonInfo["producer"].asString() + " (" + pdfInfo->GetProducer().GetStringUtf8() + ")");
    }

    for (int i=0; i<doc.GetPageCount(); i++) {
      Json::Value& pageSize = config["pageSizes"][i];
      int width = round(pageSize[0].asFloat() * 72 / 96);
      int height = round(pageSize[1].asFloat() * 72 / 96);


      PdfPage* page = doc.GetPage(i);

      PdfRect cropRect = page->GetCropBox();
      cropRect.SetBottom(cropRect.GetHeight() - height);
      cropRect.SetWidth(width);
      cropRect.SetHeight(height);

      PoDoFo::PdfObject cropRectObj;
      cropRect.ToVariant(cropRectObj);
      page->GetObject()->GetDictionary().AddKey(PoDoFo::PdfName("CropBox"), cropRectObj);
      page->GetObject()->GetDictionary().AddKey(PoDoFo::PdfName("MediaBox"), cropRectObj);
    }

    if (config["bookmarks"].isArray()) {
      PdfOutlines* outlines = doc.GetOutlines(true);
      bookmarksToOutlines(&doc, outlines, config["bookmarks"]);
    }

  if (outputFile == "-") {
      #ifdef _WIN32
        _setmode(_fileno(stdout), _O_BINARY);
        cout.sync_with_stdio();
      #endif
      PdfOutputDevice pdfOutput (&cout);
      doc.Write(&pdfOutput);
      cout.flush();
    } else {
      doc.Write (outputFile.c_str());
    }
  } catch (PdfError & e) {
    cerr << "An error " << e.GetError() << " occured while parsing the PDF.";
    e.PrintErrorMsg();
    return e.GetError();
  }

  return 0;
}
